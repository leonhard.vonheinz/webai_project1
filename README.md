# WEB-AI Search Engine

This is Group 19's Project 1 **Search Engine**.

## Authors and acknowledgment
Alexander Gerhard Ditz

Nils Niehaus

Leonard von Heiz

## Description

This 'python' project consists of:

- A **Webscraper** that crawls and indexes URLs  linked from a starting URL (crawler.py)
- A **Flask app** serving a search-interface and a results-interface (searchengine.py)

## Requirements

You need a 'python' environment with the packages:
- 'requests'
- 'beautifulsoup4'
- 'whoosh'
- 'flask'
installed,
as well as an internet connection.

## Usage

To use the search engine from your local directory, **run 'flask --app searchengine run'** in the terminal.

Since the scraped index generated only on start-up
(part of assignment), **wait a few minutes, until the terminal output confirms the app is running, and on which port.**

Open the local port in your browser and you can use the interface like other search functions you are used to. 
Enter search-terms like 'unicorn' into the text-input field and click search, to receive a list of URLs of sites containing that keyword.

## Features

This app crawls,
checking for content/response validity,
and indexes static html sites by keywords in a 'whoosh' index.

The 'index' is used by 'crawler.search()' to access a list of 
matching, indexed websites, correcting for minor misspellings
using whoosh functions

## Implementation

Running the flask app loads 'crawler.py' and executes 'crawler.main()' on startup.

The intended use-case is a focused showcase of crawling, indexing, parsing, serving, and look-up functionality.

The scraped search-index is not persistent and is generated in memory at runtime.
For this single-use one-pot use-case, we chose to implement all search-logic in one python script, 
without classes. 
(For a scalable project version with multiple crawling instances we would have used classes)

Running 'crawler.py' initializes data-structures that the functions share and operate on with side-effects, which we only did for this simple case.
We initialize:
- a Link-Traversal Stack 'to_visit', with 'collections.deque()'
- a Visit-History List 'visited' (to avoid backtracking), as an empty array
- a 'start_url'
- Irrelevant words to filter

## Gitlab

https://gitlab.gwdg.de/leonhard.vonheinz/webai_project1


