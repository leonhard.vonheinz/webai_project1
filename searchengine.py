import flask
import crawler
import traceback

#===============================================================================
# Main searchengine flask app. 
#===============================================================================


app = flask.Flask(__name__)

start_url="https://vm009.rz.uos.de/crawl/index.html"
engine = crawler.yatwoo()
engine.start(start_url)

@app.route("/")
def start():
    """
    Start window
    Runs the crawler for preprocessing.

    Returns:
        Opens the Start.html template for starter window.      
    """

    #engine = crawler.main()
    #engine.start(start_url)
    return flask.render_template("start.html")

@app.route("/search")
def search():
    """
    Search function for a term, that has been entered in text field

    Returns:
        Opens the search.html template that shows all results for a given query        
    """

    searchTerms = flask.request.args["query"]
    results, corrected_msg = engine.search(searchTerms.casefold().split())
    return flask.render_template("search.html", searchTerms = searchTerms, results = results, corrected_msg = corrected_msg)


@app.errorhandler(500)
def internal_error(exception):
   return "<pre>"+traceback.format_exc()+"</pre>"
    
