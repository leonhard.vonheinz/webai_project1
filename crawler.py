import requests
import bs4 #beautifulsoup4
from whoosh.fields import *
from whoosh import scoring
from whoosh.qparser import QueryParser
from whoosh.index import create_in
from urllib.parse import urlparse, urljoin
from collections import deque


#===============================================================================
# Crawler: Preprocessing for search enigine. URL preprocessing, 
# indexing, html parsing and more 
#===============================================================================

class yatwoo: 

    def __init__(self):
        """
        Initialize Crawler object, containing all relevant variables and states.

        Args:
            -
        Returns:
            -    
        """
        # List of excluded common words/strings
        self.exclude_words = ["", "\n", "the", "to", "this", "of", "is", "are", "a", 
                        "an", "they", "them", "he", "she", "it", 
                        "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

        # Stack of crawler 
        self.to_visit = deque()
        # Visited urls
        self.visited = []


        # Indexing shema
        schema = Schema(url=TEXT(stored=True), title=TEXT(stored=True), teaser=TEXT(stored=True), content=TEXT)

        # Initialize indexing and writer
        self.ix = create_in("indexdir", schema)
        self.writer = self.ix.writer()



    def update_index(self, soup, url):
        """
        Parses/processes BeatifulSoup object, 
        Extracts keywords and adds content, title, and teaser 
        to url index via whoosh writer.

        Args:
            soup (BeautifulSoup): parsed content of document
            url (str): current url to index

        Returns:
            -  
        
        """

        # Fetch and concat text from tags
        text_tags = soup.find_all(['p', 'a', 'title', 'div', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'])
        text = "" 
        for tag in text_tags:
            text += " " + tag.text

        title_tags = soup.find('title')
        title = "" 
        for tag in title_tags:
            title += " " + tag.text


        # text without titles for teaser text
        teaser = text.split(" ")[len(title.split(" ")):]

        # make lowercase and remove unwanted symbols
        text = text.casefold().replace(",","").replace(".", "").replace("\n", "")

        # finish website content in word list
        words = text.split(" ")
        words = [word for word in words if word not in self.exclude_words]
        
        # index URL
        self.writer.add_document(url = url, title = title, teaser = teaser ,content = str(words))


    def traverse_links(self, soup, url, domain):
        """
        Updates Crawler stack with links in current document.
        Skips visited links and external links.

        Args:
            soup (BeautifulSoup): parsed document object
            url (str): current url
            domain (str): -> hostname of start_url for constrained dev
        Returns:
            -
        """
    
        # Compile and process document-links
        links = soup.find_all('a', href=True)
        for link in links:

            l_href = abs_link(link.get('href'), url)

            # Criteria to push links:
            ###

            # visited before
            if l_href in self.visited:
                continue
            # Catches start domain (for small implementation)
            if urlparse(l_href).netloc != domain:
                continue
            
            self.to_visit.append(l_href)




    def crawl_step(self):
        """
        Pop first url from stack and call crawling functions in order.
        
        Args:
            -
        Returns:
            -
        """
        # Pop current url and domain
        url = self.to_visit.pop()
        domain = urlparse(url).netloc

        # check if visited in the meantime (not redundant with check in traverse_links())
        if url in self.visited:
            return

        # Mark current URL as visited
        self.visited.append(url)

        # Fetch and validate response
        response = requests.get(url, headers = {'Accept-Language':'English'}, timeout=5)
        if not validate_response(response):
            return

        # Parse html
        soup = bs4.BeautifulSoup(response.text, 'html.parser')
        
        # Process html content to index
        self.update_index(soup, url)    

        # Traverse links
        self.traverse_links(soup, url, domain)


    def search(self, search_terms):
        """
        Searches for matches via whoosh search for keywords in search_terms.
        Checks for spelling mistakes and searches again if necessary.
        Documents/Search-results weighed and ranked on frequency of term in site-content.

        Args:
            search_terms : list of strings

        Returns: 
            results = list of dictionary (including title, teaser text and url) for each match found

        """
        results = []
        corrected_msg = ""
        for term in search_terms:
            with self.ix.searcher(weighting = scoring.Frequency) as searcher:
                query = QueryParser("content", self.ix.schema).parse(term)
                termresults = searcher.search(query)

                # No result found. Checking for mistake and search again.
                if not termresults:
                    corrected_query = searcher.correct_query(query, term)
                    corrected_msg = "No results for " + term + ". Showing results for " + corrected_query.string + " instead:"
                    term = corrected_query.string
                    query = QueryParser("content", self.ix.schema).parse(term)
                    termresults = searcher.search(query)

                    # No result found
                    if not termresults:
                        corrected_msg = "No results found!"
                        return results, corrected_msg

                termresults = [dict(hit) for hit in termresults]

                # Get teaser text by checking if the search term is ...
                for hit in termresults:
                    term_index = [word.casefold().replace(",","").replace(".", "").replace("\n", "") for word in hit["teaser"]].index(term)
                    # at the beginning of the content
                    if term_index < 8:
                        hit['teaser'] = ' '.join(hit['teaser'][0 : 8])
                        hit['teaser'] = hit['teaser'] + '...'
                    # at the end of the content
                    elif len(hit['teaser']) - term_index < 8:
                        hit['teaser'] = ' '.join(hit['teaser'][term_index-8 : len(hit['teaser'])-1])
                        hit['teaser'] = '...' + hit['teaser']
                    # (very little content)
                    elif len(hit['teaser']) - term_index < 8 and term_index <= 8:
                        hit['teaser'] = ' '.join(hit['teaser'][0 : len(hit['teaser'])-1])
                    # in the middle of a sufficiently long content
                    else:
                        hit['teaser'] = ' '.join(hit['teaser'][term_index-8 : term_index+8])
                        hit['teaser'] = '...' + hit['teaser'] + '...'

                # Concatenate results from the different terms    
                results += termresults
        return results, corrected_msg

    def start(self, start_url):
        """
        Push start-url to stack and loop crawl_step() until stack empty or too big.
        (Stack max 10 is used for example implementation)

        Args:
            start_url (str): starting url for crawler
        """

        # Loop over Crawl-stack
        self.to_visit.append(start_url)
        while len(self.to_visit) != 0:
            if len(self.to_visit)>10:
                break
            self.crawl_step()
        self.writer.commit()


#===============================================================================
# Auxilliary Functions
#===============================================================================


def abs_link(l_href, url):
    """
    Joins domain-relative links with base urls to 'absolute links'.
    Used by traverse_links()

    Args:
        str l_href: link (possibly relative to domain)
        str url: base url

    Returns:
        str l_href: link asserted to be absolute    
    """
    
    if urlparse(l_href).netloc == "":
        l_href = urljoin(url, l_href)
    return l_href


def validate_response(response):
    """
    Checks a response object for desired criteria:
    -Status code 200
    -html content-type

    Args:
        response: server response to request.get(...)

    Returns:
        valid (bool): value of evaluated conditions        
    """

    valid = True

    # Check Statuscode 
    if response.status_code != 200:
        valid = False
        print(f"Invalid Server Response!\n{response}")

    # Check Content-Type
    if 'text/html' not in response.headers['Content-Type']:
        valid = False
        print(f"Content type {response.headers['Content-Type']} not supported by this implementation!")

    return valid    



    